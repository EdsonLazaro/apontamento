﻿Imports System.Net

Public Class Retorna_Pesquisa
    Dim Client As New WebClient
    Dim ClientUTF As New WebClient
    Public Sub New()
        Client.Encoding = System.Text.Encoding.UTF8
        ClientUTF.Encoding = System.Text.Encoding.GetEncoding("ISO-8859-1")
    End Sub

    '<<<<<<<<<<<<<<<<<<<<<<<< No dia 29/10/2020 o apontamento das requisições do WSINFOCAR, foram alterados de http://cadastro.info/ROBOS82/ para "http://novocadastro.info/, na 177 pedido do Daniel gomes. Edson>>

    Public Function retornaBaseNacional(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaBaseNacional As String = Client.DownloadString("http://localhost/BN-NOVA-PESQUISA\PWB7976-MOTOCONSULTA.html").ToUpper
        Dim saidaBaseNacional As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICEWSINFOCAR&senha=@01062017A1628").ToUpper
        'Dim saidaBaseNacional As String = ClientUTF.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Dim dll_leilao As New LeilaoInfocar.FuncoesLeiloes
        saidaBaseNacional = dll_leilao.tiraAcentuacao(dll_leilao.clearDebug(saidaBaseNacional))
        saidaBaseNacional = saidaBaseNacional.Replace("DABITO", "DEBITO")
        '    saidaBaseNacional = saidaBaseNacional.Replace("DÃBITO", "DEBITO").Replace("UTILITÃ RIO", "UTILITARIO").Replace("NÃƒO", "NAO").Replace("DÃ‰BITO")
        Return saidaBaseNacional
    End Function

    Public Function retornaBaseNacionalMOTOR(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaBaseNacional As String = Client.DownloadString("http://localhost/BN-NOVA-PESQUISA\PWB7976-MOTOCONSULTA.html").ToUpper
        Dim saidaBaseNacional As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICEWSINFOCAR&senha=@01062017A1628").ToUpper
        'Dim saidaBaseNacional As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaBaseNacional
    End Function

    Public Function retornaVistoria(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaVistoria As String = Client.DownloadString("http://192.168.0.183/HTML-ConferePF-Nova/base luis.xml".ToUpper
        Dim saidaVistoria As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICEWSINFOCAR&senha=@01062017A1628").ToUpper
        'Dim saidaVistoria As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaVistoria
    End Function

    Public Function retornaHistoricoVeicular(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaVistoria As String = Client.DownloadString("http://192.168.0.183/HTML-ConferePF-Nova/base luis.xml".ToUpper
        Dim saidaVistoria As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/PROPRIETARIO_CHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        'Dim saidaVistoria As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/PROPRIETARIO_CHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaVistoria
    End Function

    Public Function retornaInfocarCNH(ByVal dado As String, ByVal numero_registro As String, ByVal numero_rg As String, ByVal numero_espelho As String) As String
        'Dim saidaVistoria As String = Client.DownloadString("http://192.168.0.183/HTML-ConferePF-Nova/base luis.xml".ToUpper
        Dim saidaVistoria As String = Client.DownloadString("http://novocadastro.info/GerenciadorDeConsultas/AUTO/CNH.aspx?CPF=" & dado & "&NREGISTRO=" & numero_registro & "&NRG=" & numero_registro & "&NESPELHO=" & numero_espelho & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        'Dim saidaVistoria As String = Client.DownloadString("http://cadastro.info/GerenciadorDeConsultas/AUTO/CNH.aspx?CPF=" & dado & "&NREGISTRO=" & numero_registro & "&NRG=" & numero_registro & "&NESPELHO=" & numero_espelho & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper

        Return saidaVistoria
    End Function

    Public Function retornaBaseEstadual(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaBaseEstadual As String = Client.DownloadString("http://localhost/BN-NOVA-PESQUISA\PWB7976-MOTOCONSULTA.html").ToUpper
        Dim saidaBaseEstadual As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICEWSINFOCAR&senha=@01062017A1628").ToUpper
        'Dim saidaBaseEstadual As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaBaseEstadual
    End Function
    Public Function retornaDados_Prop_Atual(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://localhost/BN-NOVA-PESQUISA\PWB7976-RENE.html").ToUpper
        Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICEWSINFOCAR&senha=@01062017A1628").ToUpper
        'Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaDados_Prop_Atual
    End Function
    Public Function retornaHistoricoProprietario(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaHistoricoProprietario As String = Client.DownloadString("http://localhost/CheckPro-PA-HP/proprietario-RENE.html").ToUpper PROPROT.aspx
        Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/PROPROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        'Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/PROPROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaDados_Prop_Atual
    End Function
    Public Function retornaFipe(ByVal dado As String, ByVal tipo As String) As String
        Dim saidaFipe As String = Client.DownloadString("http://localhost/Teste_Fipe\Teste.html").ToUpper
        'Dim saidaFipe As String = Client.DownloadString("http://pesquisaconsultaauto.com.br/historicoproprietario.php?txtusuario=INFOCAR&txtsenha=infocarsl04x&txtplaca=" & dado").ToUpper
        Return saidaFipe
    End Function
    Public Function retornaLeilaoBase3(ByVal dado As String, ByVal tipo As String) As String
        Dim saidaLeilaoBase3 As String = Client.DownloadString("http://localhost/Base2Leilao/DDY1441.html").ToUpper
        'Dim saidaLeilaoBase3 As String = Client.DownloadString("").ToUpper
        Return saidaLeilaoBase3
    End Function
    'Public Function retornaGravame_Check_Pro(ByVal dado As String, ByVal tipo As String) As String
    '    'Dim c As String = Client.DownloadString("http://localhost/CheckPro-GVS\9BGRM69808G242467.html").ToUpper 'ConsultaVeiculo/GRAVAME_CHECKPRO.aspx
    '    Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/GRAVAME_CHECKPRO.aspx?chassi=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
    '    Return saidaDados_Prop_Atual & "<FORNECEDOR>CHECKPRO</FORNECEDOR>"
    'End Function
    Public Function retornaGravame_Check_Pro(ByVal dado As String, ByVal tipo As String) As String
        '  Dim saidaDados_Gravame As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/GravameAutoCredCar.aspx?dado=" & dado & "&tipo=CHASSI&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper 'ConsultaVeiculo/GRAVAME_CHECKPRO.aspx
        Dim saidaDados_Gravame As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/GravameAutoCredCar.aspx?dado=" & dado & "&tipo=CHASSI&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper 'ConsultaVeiculo/GRAVAME_CHECKPRO.aspx

        'ESTA CONSULTA ESTÁ BLOQUEADA NO MOMENTO
        If saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA CONSULTA AINDA NÃO ESTAR PRONTA.</MENSAGEM>") Or _
            saidaDados_Gravame.Contains("<MENSAGEM>ESTA CONSULTA SÓ PODE SER FEITA DE SEGUNDA À SEXTA DAS 07:00 ÀS 19:00</MENSAGEM>") Or _
            saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA CONSULTA AINDA NÃO ESTÁ PRONTA.</MENSAGEM>") Or _
            saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA CONSULTA AINDA NÃO ESTA PRONTA.</MENSAGEM>") Or _
            saidaDados_Gravame = "" Or saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA INDISPONÍVEL.</MENSAGEM>") Or _
            saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA INDISPONIVEL") Or _
            saidaDados_Gravame.Contains("ESTA CONSULTA ESTÁ BLOQUEADA NO MOMENTO") Or _
            saidaDados_Gravame.Contains("<MENSAGEM>CONSULTA MOMENTANEAMENTE INDISPONÍVEL</MENSAGEM>") Then
            saidaDados_Gravame = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/GRAVAME_CHECKPRO.aspx?chassi=" & dado & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
            ' Return saidaDados_Gravame & "<FORNECEDOR>CHECKPRO</FORNECEDOR>"
        End If
        Return saidaDados_Gravame
    End Function
  
    Public Function retornaRoubo_Furto(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaRoubo_Furto As String = Client.DownloadString("http://localhost/CheckPro-RouboFurto/BTA1395-2.html").ToUpper
        'RFCHECKPRO.aspx  '
        Dim saidaRoubo_Furto As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/RFCHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        '  Dim saidaRoubo_Furto As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/RFCHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaRoubo_Furto & "<FORNECEDOR>CHECKPRO</FORNECEDOR>"
    End Function
    Public Function retornaRoubo_FurtoTDI(ByVal dado As String, ByVal tipo As String) As String
        'Dim saidaRoubo_Furto As String = Client.DownloadString("http://localhost/CheckPro-RouboFurto/BTA1395-2.html").ToUpper
        'RFCHECKPRO.aspx
        Dim saidaRoubo_Furto As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/rouboefurtoxml.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        '    Dim saidaRoubo_Furto As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/rouboefurtoxml.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Return saidaRoubo_Furto & "<FORNECEDOR>TDI</FORNECEDOR>"
    End Function

    Public Function retornaRoubo_FurtoROTUSUARIO(ByVal dado As String, ByVal tipo As String) As String
        Dim saidaRoubo_Furto As String
        '    saidaRoubo_Furto = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=SIS3&senha=310320171215").ToUpper
        saidaRoubo_Furto = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=SIS3&senha=310320171215").ToUpper
        If saidaRoubo_Furto.Contains("##ERRO##") Then
            saidaRoubo_Furto = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=NAOLOC&senha=30302030").ToUpper
            '  saidaRoubo_Furto = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=NAOLOC&senha=30302030").ToUpper
            'saidaRoubo_Furto = saidaRoubo_Furto & "<FORNECEDOR>RENE</FORNECEDOR>"
        End If
        Return saidaRoubo_Furto '& "<FORNECEDOR>TDI</FORNECEDOR>" '
    End Function
    Public Function retornaRoubo_FurtoROT(ByVal dado As String, ByVal tipo As String) As String
        Dim saidaRoubo_Furto As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/RFROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        ' Dim saidaRoubo_Furto As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/RFROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        'Dim saidaRoubo_Furto As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/BNROT.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=SIS3&senha=310320171215").ToUpper
        Return saidaRoubo_Furto '& "<FORNECEDOR>TDI</FORNECEDOR>" '
    End Function

    Public Function retornaRoubo_Furto_Fenauto(ByVal dado As String, ByVal tipo As String) As String
        Dim saidaRoubo_Furto As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/RFROT.aspx?tipo=" & tipo & "&dado=" & dado & "&usuario=WEBSITE&senha=@154fgH").ToUpper
        'Dim saidaRoubo_Furto As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/RFROT.aspx?tipo=" & tipo & "&dado=" & dado & "&usuario=WEBSITE&senha=@154fgH").ToUpper
        Return saidaRoubo_Furto '& "<FORNECEDOR>TDI</FORNECEDOR>" 'cadastro.info/ROBOS82/ConsultaVeiculo/RFROT.aspx?tipo=PLACA&dado=YMH0125&usuario=WEBSITE&senha=@154fgH
    End Function


    Public Function retorna_Restricao_Total_PF(ByVal dado As String, ByVal tipo As String) As String
        Dim saida_Restricao_Total_PF As String = Client.DownloadString("http://localhost/RestricaoPFScore/26976016864.html").ToUpper
        'Dim saida_Restricao_Total_PF As String = Client.DownloadString("http://pesquisaconsultaauto.com.br/historicoproprietario.php?txtusuario=INFOCAR&txtsenha=infocarsl04x&txtplaca=" & dado").ToUpper
        Return saida_Restricao_Total_PF
    End Function
    Public Function retorna_PFIN_PF(ByVal dado As String, ByVal tipo As String) As String
        Dim saida_PFIN_PF As String = Client.DownloadString("http://localhost/PFIN/01234567890.html").ToUpper
        'Dim saida_PFIN_PF As String = Client.DownloadString("http://pesquisaconsultaauto.com.br/historicoproprietario.php?txtusuario=INFOCAR&txtsenha=infocarsl04x&txtplaca=" & dado").ToUpper
        Return saida_PFIN_PF
    End Function
    Public Function retorna_Confere_PF(ByVal dado As String, ByVal tipo As String) As String
        Dim saida_Confere_PF As String = Client.DownloadString("http://localhost/PesquisaConfere/Confere2.html").ToUpper
        'Dim saida_Confere_PF As String = Client.DownloadString("http://pesquisaconsultaauto.com.br/historicoproprietario.php?txtusuario=INFOCAR&txtsenha=infocarsl04x&txtplaca=" & dado").ToUpper
        Return saida_Confere_PF
    End Function
    Public Function retorna_Confere_Info_Comp_PF(ByVal dado As String, ByVal tipo As String) As String
        Dim saida_Confere_Info_Comp_PF As String = Client.DownloadString("http://localhost/PesquisaConfere/01234567890.html").ToUpper
        '  Dim saida_Confere_Info_Comp_PF As String = Client.DownloadString("http://pesquisaconsultaauto.com.br/historicoproprietario.php?txtusuario=INFOCAR&txtsenha=infocarsl04x&txtplaca=" & dado").ToUpper
        Return saida_Confere_Info_Comp_PF
    End Function
    Public Function retornaPerdaTotal(ByVal dado As String, ByVal tipo As String) As String
        '   Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://cadastro.info/ROBOS82/ConsultaVeiculo/PTCHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper
        Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://novocadastro.info/ConsultaVeiculo/PTCHECKPRO.aspx?dado=" & dado & "&tipo=" & tipo & "&usuario=WEBSERVICE&senha=@01062017A1628").ToUpper

        Return saidaDados_Prop_Atual & "<FORNECEDOR>CHECKPRO</FORNECEDOR>"
    End Function

    Public Function retornaInfocarRecall(ByVal dado As String) As String
        Dim saidaDados_Prop_Atual As String = Client.DownloadString("http://precodocarro.com.br/RECALL/pRecall.aspx?dado=" & dado).ToUpper

        Return saidaDados_Prop_Atual
    End Function
    Public Function retornaParecerTecnico(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">  <soap:Body>    <ParecerTecnico xmlns='http://tempuri.org/'><dado>{0}</dado><tipo>{1}</tipo><chave>{2}</chave></ParecerTecnico>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSPARTEC/Parecer.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/ParecerTecnico""")

        Dim saidaParecer As String = String.Empty
        saidaParecer = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper()



        Return saidaParecer & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function
    Public Function retornaFIPEXML(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <placa>{0}</placa>      <chassi>{1}</chassi>      <tipo>{2}</tipo>      <OPCAO>158</OPCAO>      <chave>{3}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSCOD578/WSCODF483.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")

        Dim saidaAgregados As String = String.Empty
        If tipo.ToUpper = "PLACA" Then
            saidaAgregados = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, "", "0", chave)).ToUpper()
        Else
            saidaAgregados = client.UploadString(linkWs, "POST", String.Format(envio_soap, "", dado, "1", chave)).ToUpper()
        End If


        Return saidaAgregados & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function


    Public Function retornaFIPEXMLFAKE(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <placa>{0}</placa>      <chassi>{1}</chassi>      <tipo>{2}</tipo>      <OPCAO>158</OPCAO>      <chave>{3}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSCOD578/WSCODF483.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")

        Dim saidaAgregados As String = String.Empty
        If tipo.ToUpper = "PLACA" Then
            saidaAgregados = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, "", "0", chave)).ToUpper()
        Else
            saidaAgregados = client.UploadString(linkWs, "POST", String.Format(envio_soap, "", dado, "1", chave)).ToUpper()
        End If


        Return saidaAgregados & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function



    Public Function retornaAgregados(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
    "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSAGREG1548/A1548.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")

        Dim saidaAgregados As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaAgregados & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function


    Public Function retornaAgregadosFAKE(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
    "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSAGREG1548/A1548.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")

        Dim saidaAgregados As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaAgregados & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function

    Public Function retornaLeilaoCNVL35(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
            "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "https://datacast.info/WSCNVL35/L0934.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")

        Dim saidaLeilao As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaLeilao & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function

    Public Function retornaLeilaoCNVL34(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
        "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "http://datacast.info/WSCNVL34-3/L0934F.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")
        Dim saidaLeilaoSeguradoras As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaLeilaoSeguradoras & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function
    Public Function retornaRemarketing(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
        "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"

        Dim linkWs As String = "https://datacast.info/WSHOV/H0836.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")
        Dim saidaRemark As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaRemark & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function
    Public Function retornaIndicioSinistro(ByVal dado As String, ByVal tipo As String) As String
        Dim envio_soap As String = "<?xml version=""1.0"" encoding=""utf-8""?><soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &
        "  <soap:Body>    <consulta xmlns=""http://tempuri.org/"">      <dado>{0}</dado>      <tipo>{1}</tipo> <chave>{2}</chave>    </consulta>  </soap:Body></soap:Envelope>"
        Dim linkWs As String = "https://datacast.info/WSINDSIS/I1151.asmx" 'ABK3449
        Dim usuario As String = "INFOCAR"
        Dim senha As String = "IF052A896"
        Dim chave As String = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(usuario & ":" & senha))
        Dim client As New WebClient

        client.Encoding = System.Text.Encoding.UTF8
        client.Headers.Add("Content-Type", "text/xml; charset=utf-8")
        client.Headers.Add("SOAPAction", """http://tempuri.org/consulta""")
        Dim saidaIndicio As String = client.UploadString(linkWs, "POST", String.Format(envio_soap, dado, tipo, chave)).ToUpper

        Return saidaIndicio & "<FORNECEDOR>INFOCAR</FORNECEDOR>"
    End Function

End Class
